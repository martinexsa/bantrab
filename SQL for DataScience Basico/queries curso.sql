-- CONSULTA DE DATOS
-- select id, nombre, promedio
-- from estudiante 
-- where promedio > 90;

-- select distinct nombre, materia
-- select nombre, materia
-- from estudiante, asignacion
-- where estudiante.id = asignacion.estudiante_id
-- AND nombre = 'Ignacia';

-- select * 
-- from universidad
-- where universidad LIKE '%Ante Incorporated%';
-- select nombre, nota 
-- from estudiante, asignacion
-- where estudiante.id = asignacion.estudiante_id
-- AND materia = 'D'
-- AND universidad_id = 55;

-- select estudiante.id, nombre, promedio, universidad
-- from estudiante, asignacion, universidad
-- where estudiante.id = asignacion.estudiante_id 
-- and asignacion.universidad_id = universidad.id
-- order by promedio desc, universidad;

-- select * from universidad where universidad ilike '%CUM%';

-- CONJUNTOS
-- Select estudiante.id, nombre, apellido, promedio, universidad, telefono
-- From estudiante, universidad, asignacion
-- Where asignacion.estudiante_id = estudiante.id
-- and asignacion.universidad_id = universidad.id;
-- Select e.id, nombre, apellido, promedio, universidad, telefono
-- From estudiante e, universidad u, asignacion a
-- Where a.estudiante_id = e.id
-- And a.universidad_id = u.id

-- SUBCONSULTAS
-- Select id, nombre
-- From estudiante
-- Where id in (select estudiante_id from asignacion where materia = 'B');
-- Select e.id, nombre
-- From estudiante e, asignacion a
-- Where e.id = a.estudiante_id
-- And materia = 'B' ORDER BY e.id

-- Select id, nombre
-- From estudiante
-- Where id in ( select estudiante_id from asignacion where materia = 'D')
-- And id not in (select estudiante_id from asignacion where materia = 'A')
-- order by id
-- Select id, nombre
-- From estudiante 
-- Where id in ( select estudiante_id from asignacion where materia = 'B')
-- And not id in (select estudiante_id from asignacion where materia = 'A')
-- order by id

-- Select universidad, direccion
-- From universidad u1
-- Where exists (select * from universidad u2 where u2.direccion = u1.direccion) 
-- order by direccion;
-- select universidad, direccion
-- From universidad u1
-- Where exists (select * 
-- 				from universidad u2 
-- 				where u2.direccion = u1.direccion and u1.id <> u2.id) 
-- order by direccion;


-- JOINS
-- Select distinct nombre, materia
-- From estudiante, asignacion
-- Where estudiante.id=asignacion.estudiante_id;
-- Select distinct nombre, materia
-- From estudiante inner join asignacion 
-- On estudiante. Id = asignacion.estudiante_id;
-- Select distinct nombre, materia
-- From estudiante join asignacion 
-- On estudiante. Id = asignacion.estudiante_id;


-- select count(*) 
-- from estudiante e left join asignacion a on (a.estudiante_id = e.id)
-- where materia is null;

-- select nombre, estudiante.id, asignacion.universidad_id, materia, asignacion.estudiante_id
-- from estudiante full  join asignacion using(id);




-- AGGS
-- Select avg(promedio), count(*)
-- From estudiante, asignacion
-- Where estudiante.id = asignacion.estudiante_id
-- AND materia = 'C';
-- select estudiante_id, materia, nota from asignacion where materia = 'C'
-- order by estudiante_id;
-- Select count(*), avg(promedio)
-- From estudiante
-- Where estudiante.id IN 
-- 	(select estudiante_id from asignacion where materia = 'C');

-- select *
-- from asignacion
-- where universidad_id = 55;

-- Select u.universidad, a.materia, min(nota) as nota_menor, max(nota) as nota_mayor
-- FROM asignacion a JOIN universidad u ON (u.id = a.universidad_id)
-- GROUP BY u.id, a.materia;

-- select e.id, u.universidad
-- From estudiante e JOIN asignacion a ON (e.id = a.estudiante_id)
-- JOIN universidad u ON (u.id = a.universidad_id)
-- order by e.id;
-- Select e.id, count(distinct u.universidad)
-- From estudiante e JOIN asignacion a ON (e.id = a.estudiante_id)
-- JOIN universidad u ON (u.id = a.universidad_id)
-- GROUP BY e.id;


-- select u.universidad, count(*)
-- FROM asignacion a JOIN universidad u ON (u.id = a.universidad_id)
-- group by u.universidad
-- having count(*) > 3
-- order by count


